<?php
session_start();

if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}
$_SESSION['init'] = true;

if (!empty($_POST)) {
    if (isset($_POST['auth']) and isset($_POST['auth-password'])) {
        $authLogin = $_POST['auth'];
        $authPass = $_POST['auth-password'];
        $auth = trim($authLogin);
        $authPass = trim($authPass);
        $authPass =  hash('sha512', $authPass);

        require_once "controller/userController.php";
        $authUser = new userController();
        $authResult = $authUser->checkUser($authLogin);
        $loggedUser = $authResult['username'];
        $loggedPass = $authResult['password'];
        $loggedRole = $authResult['role'];

        if ($authLogin == $loggedUser and $authPass == $loggedPass) {
            $_SESSION['username'] = $loggedUser;
            $_SESSION['role'] = $loggedRole;
            $_SESSION['logged'] = true;
        } else {
            $authError = "<p>Incorrect login or password!</p>";
            $_SESSION['authError'] = $authError;
        }
    }
    if (isset($_POST['login']) and isset($_POST['password']) and isset($_POST['re-password'])) {
        $newUser = $_POST['login'];
        $password = $_POST['password'];
        $rePassword = $_POST['re-password'];
        $role = $_POST['role'];
        $newUser = trim($newUser);

        if (trim($password) == trim($rePassword)) {
            require_once "controller/userController.php";
            $putUser = new userController();
            $checkedUser = $putUser->checkUser($newUser);
            $checkedUser = $checkedUser['username'];
            if ($checkedUser == $newUser) {
                echo "<p>This user exist!</p>";
            }
            else if ($checkedUser != $newUser) {
                $putUser->addNewU($newUser, $password, $role);
            }
        } else {
            echo "Wrong password";
        }
    }

    if(isset($_POST['editSend'])){
        $userEdit=$_POST['editLogin'];
        $editRole = $_POST['editRole'];
        require_once 'controller/userController.php';
        $editUser = new userController();
        $editUser->editUser($userEdit, $editRole);
    }

    if (isset($_POST['delete'])) {
        $deleteLogin = $_POST['deleteLogin'];
        if ($deleteLogin != 'admin') {
                require_once "controller/userController.php";
                $deleteUser = new userController();
                $unsetUser = $deleteUser->delete($deleteLogin);
            } else {
                echo "<p>You can't delete this superuser!</p>";
            }
    }
}
?>
<script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>
<?php
require_once "view/header.php";

?>


<main id="admin">
    <div class="container-fluid">
        <div class="container">
        <div class="row">
            <?php
            if (!empty($_SESSION)) {
                if (!isset($_SESSION['logged'])) {
                    ?>
                    <div class="col-xs-12">
                        <h2>Authorization</h2>
                        <form action="admin.php" method="post">
                            <div class="form-group">
                                <label for="login">Login</label>
                                <input name="auth" type="text" class="form-control" placeholder="Login for new user">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input name="auth-password" type="password" class="form-control" placeholder="Password">
                            </div>
                            <button name="send-auth" type="submit" class="btn btn-default">LOG IN</button>
                        </form>
                        <?php
                        if (!empty($_SESSION)) {
                            if (isset($_SESSION['authError'])) {
                                echo $_SESSION['authError'];
                                unset($_SESSION['authError']);
                            }
                        }
                        ?>
                        <hr>
                    </div>
                    <?php
                }
                else {
                    echo '<h4>Welcome,' . $_SESSION['username'] . "</h4>";
                }
            }
            ?>
        </div>
            <?php
            if (!empty($_SESSION)) {
                if (isset($_SESSION['logged']) and $_SESSION['role'] == 'admin') {
                    ?>
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addUser">
                            Add new User
                </button>
                    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add user</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="admin.php" method="post">
                                        <div class="form-group">
                                            <label for="login">Login</label>
                                            <input name="login" type="text" class="form-control"
                                                       placeholder="Login for new user">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input name="password" type="password" class="form-control"
                                                       placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="re-password">Retype-Password</label>
                                            <input name="re-password" type="password" class="form-control"
                                                       placeholder="Retype Passwordrd">
                                        </div>
                                        <select class="select" name="role">
                                            <option value="user">user</option>
                                            <option value="moderator">Moderator</option>
                                            <option value="admin">Admin</option>
                                        </select>
                                            <?php
                                            if (!empty($_SESSION)) {
                                                if (isset($_SESSION['userErrorMessage'])) {
                                                    echo $_SESSION['userErrorMessage'];
                                                    unset($_SESSION['userErrorMessage']);
                                                }
                                            }
                                            ?>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Close
                                            </button>
                                            <button name="send" type="submit" class="btn btn-primary"> Add user
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                        <?php
                        if (!empty($_SESSION)) {
                            if (isset($_SESSION['loginError'])) {
                                echo $_SESSION['loginError'];
                                unset($_SESSION['loginError']);
                            }
                            if (isset($_SESSION['userMessage'])) {
                                echo $_SESSION['userMessage'];
                                unset($_SESSION['userMessage']);
                            }
                            if (isset($_SESSION['userErrorMessage'])) {
                                echo $_SESSION['userErrorMessage'];
                                unset($_SESSION['userErrorMessage']);
                            }
                            if (isset($_SESSION['checkedUser'])) {
                                echo $_SESSION['checkedUser'];
                                unset($_SESSION['checkedUser']);
                            }
                        }
                        ?>
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#deleteModal">
                        Delete user
                </button>

                    <!-- Modal Delete -->
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete user</h4>
                            </div>
                            <div class="modal-body">
                                <form action="admin.php" method="post">
                                    <div class="form-group">
                                        <label for="login">Login</label>
                                            <select name="deleteLogin">
                                                <?php
                                                require_once "controller/userController.php";
                                                $option = new userController();
                                                $allOptions=$option->getAllUsers();

                                                foreach ($allOptions as $option) {
                                                    ?>
                                                  <option value="<?=$option['username']?>"><?=$option['username']?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button name="delete" type="submit" class="btn btn-primary">Delete user</button>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                        data-target="#updateModal">
                                Edit role
                </button>
                            <!-- Modal Edit -->
                <div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit role</h4>
                            </div>
                            <div class="modal-body">
                                <form action="admin.php" method="post">
                                    <div class="form-group">
                                        <label for="login">Login</label>
                                        <select name="editLogin">
                                            <?php
                                            require_once "controller/userController.php";
                                            $option = new userController();
                                            $allOptions=$option->getAllUsers();

                                            foreach ($allOptions as $option) {
                                                ?>
                                                <option value="<?=$option['username']?>"><?=$option['username']?> </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <select class="select" name="editRole">
                                        <option value="user">user</option>
                                        <option value="moderator">Moderator</option>
                                        <option value="admin">Admin</option>
                                    </select>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
                                        <button name="editSend" type="submit" class="btn btn-primary">Edit role </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <?php
             }
            }

            ?>

         </div>
    </div>
</main>

<?php
require_once "view/footer.php";
?>
