<?php
session_start();

    if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
        session_unset();
    }
    $_SESSION['init'] = true;
    require_once "controller/blogController.php";

if (!empty($_POST)) {

    // -----Add post---------
    if (isset($_POST['sendPost'])) {
        $title = $_POST['title'];
        $category=$_POST['category'];
        $excerpt = $_POST['excerpt'];
        $fullPost = $_POST['fullPost'];
        $author = $_SESSION['username'];
        $show=$_POST['show'];

        if ((!empty($_FILES))) {
            if (!empty($_FILES['fileToUpload']['name'])) {
                $target_dir = "assets/uploads/";
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if ($check !== false) {
                    $target_file = $target_dir . $_FILES["fileToUpload"]["name"];
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
                }
                else {
                    echo "File is not an image.";
                }
                // Check file size
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";

                }
            }
            else {
                $target_file = "assets/uploads/default.jpg";
            }
        }
        $addPost = new blogController();
        $addPost->addPost($title,$category, $excerpt, $fullPost, $author, $target_file, $show);
        unset($target_file);
        unset($_FILES["fileToUpload"]);
    }

    if (isset($_POST['deletePost'])) {

        $delPost = $_POST['deletePost'];
        $deletePost = new blogController();
        $deletePost->delPost($delPost);
    }

    if (isset($_POST['editPostSend'])) {
        $id=$_POST['editPostSend'];
        $editTitle=$_POST['editTitle'];
        $editCategory=$_POST['editCategory'];
        $editExcerpt=$_POST['editExcerpt'];
        $editFullPost = $_POST['editFullPost'];


        if ((!empty($_FILES))) {
            if (!empty($_FILES['editFileToUpload']['name'])) {
                $target_dir = "assets/uploads/";
                $check = getimagesize($_FILES["editFileToUpload"]["tmp_name"]);
                if ($check !== false) {
                    $target_file = $target_dir . $_FILES["editFileToUpload"]["name"];
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES["editFileToUpload"]["tmp_name"], $target_file);
                }
                else {
                    echo "File is not an image.";
                }
                // Check file size
                if ($_FILES["editFileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";

                }
            }
            else {
                $editTarget_file=$_POST['imgDb'];
            }
        }
        $editShow=$_POST['editShow'];
        $editP = new blogController();
        $editP->editPostF($id, $editTitle, $editCategory, $editExcerpt, $editFullPost, $editTarget_file, $editShow);
        unset($editTarget_file);
        unset($_FILES["editFileToUpload"]);
    }

    if (isset($_POST['id']) and isset($_POST['show'])) {
            $id=$_POST['id'];
            $show=$_POST['show'];
            $editShow=new blogController();
            $editShow->changeShow($id, $show);
    }
    if (isset($_POST['sendContact'])) {
        $title = $_POST['title'];
        $address = $_POST['address'];
        $tel = $_POST['tel'];
        $show = $_POST['show'];
        $addPost = new blogController();
        $addPost->addContact($title, $address, $tel, $show);
    }

}

require_once "view/header.php";
?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
<main>
    <div class="container-fluid">
        <div class="container">
            <?php
            if (!empty($_SESSION)) {
                if (!isset($_SESSION['logged'])) {
            ?>
            <div class="col-xs-12">
                <h2>Authorization</h2>
                <form action="admin.php" method="post">
                    <div class="form-group">
                        <label for="login">Login</label>
                        <input name="auth" type="text" class="form-control" placeholder="Login for new user">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input name="auth-password" type="password" class="form-control" placeholder="Password">
                    </div>
                    <button name="send-auth" type="submit" class="btn btn-default">LOG IN</button>
                </form>
                <?php
                if (!empty($_SESSION)) {
                    if (isset($_SESSION['authError'])) {
                        echo $_SESSION['authError'];
                        unset($_SESSION['authError']);
                    }
                }
                ?>
                <hr>
            </div>
            <?php
                }
            }
            ?>



            <?php
            if (!empty($_SESSION)) {
                if (isset($_SESSION['logged'])) {

                    if (($_SESSION['role'] == 'admin') or ($_SESSION['role'] == 'moderator')) {
                        $count = 10;
                        $allPages = true;
                        $post = new blogController();
                        $allPosts = $post->writePosts($start = 0, $count);
                        $countAllPosts = $post->countPosts();
                        $postsCount = count($countAllPosts);
                        $len = ceil($postsCount / $count);

                        if ((isset($_GET['page']))) {

                            $page = $_GET['page'];
                            for ($page == 1; $page <= $len; $page++) {
                                $start = ($page - 1) * $count;
                                $allPosts = $post->writePosts($start, $count);
                                break;
                            }
                        }

                        if (!empty($_GET['post_author'])) {
                            $allPages = false;
                            $postAuthor = $_GET['post_author'];
                            $allPosts = $post->sortPostAuthor($postAuthor, $start = 0, $count);
                            $countAuthorPosts = $post->countPostsByAuthor($postAuthor);
                            $postsAuthorCount = count($countAuthorPosts);
                            $lenByAuthor = ceil($postsAuthorCount / $count);

                            if (isset($_GET['page']) && !empty($_GET['page']) AND isset($_GET['post_author']) && !empty($_GET['post_author'])) {
                                for ($page == 1; $page <= $lenByAuthor; $page++) {
                                    $start = ($page - 1) * $count;
                                    $allPosts = $post->sortPostAuthor($postAuthor, $start, $count);
                                    break;
                                }
                            }
                        }

                        if (!empty($_GET['post_category'])) {
                            $allPages = false;
                            $postCategory = $_GET['post_category'];
                            $allPosts = $post->sortPostCategory($postCategory, $start = 0, $count);
                            $countCategoryPosts = $post->countPostsByCategory($postCategory);
                            $postsCategoryCount = count($countCategoryPosts);
                            $lenByCategory = ceil($postsCategoryCount / $count);

                            if (isset($_GET['page']) && !empty($_GET['page']) AND isset($_GET['post_category']) && !empty($_GET['post_category'])) {
                                for ($page == 1; $page <= $len; $page++) {
                                    $start = ($page - 1) * $count;
                                    $allPosts = $post->sortPostCategory($postCategory, $start, $count);
                                    break;
                                }
                            }
                        }
                        ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <button type="button" class="btn btn-primary btn-lg newpost" data-toggle="modal"
                                        data-target="#addPost">
                                    Add new post
                                </button>
                                <div class="modal fade" id="addPost" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Add post</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="adminParser.php" method="post"
                                                      enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="title">Title</label>
                                                        <input name="title" type="text" class="form-control"
                                                               placeholder="Title of post">
                                                    </div>
                                                    <select class="select" name="category">
                                                        <option value="Правова допомога">Правова допомога</option>
                                                        <option value="Психологічна підтримка">Психологічна підтримка
                                                        </option>
                                                        <option value="Інша">Інша</option>
                                                    </select>
                                                    <div class="radio">
                                                        <label><input type="radio" name="show" value="hide" checked>
                                                            Hide</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="show" value="show">
                                                            Show</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="excerpt">Excerpt</label>
                                                        <textarea name="excerpt" rows="4" cols="75"> </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="fullPost">FullPost</label>
                                                        <textarea name="fullPost" id="fullPost" rows="20"
                                                                  cols="60"></textarea>
                                                        <script type="text/javascript">
                                                            CKEDITOR.replace('fullPost');
                                                        </script>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="img">Select image to upload:</label>
                                                        <input type="file" name="fileToUpload" id="fileToUpload">
                                                    </div>

                                                    <?php
                                                    if (!empty($_SESSION)) {
                                                        if (isset($_SESSION['postMessage'])) {
                                                            echo $_SESSION['postMessage'];
                                                            unset($_SESSION['postMessage']);
                                                        }

                                                        if (isset($_SESSION['postErrorMessage'])) {
                                                            echo $_SESSION['postErrorMessage'];
                                                            unset($_SESSION['postErrorMessage']);
                                                        }
                                                    }
                                                    ?>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal"> Close
                                                        </button>
                                                        <button name="sendPost" type="submit" class="btn btn-primary">
                                                            Add post
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                ?>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <button type="button" class="btn btn-primary btn-lg newpost" data-toggle="modal"
                                        data-target="#addContacts">
                                    Add new contacts
                                </button>
                                <div class="modal fade" id="addContacts" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Add contact</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="adminParser.php" method="post" name="addContact">
                                                    <div class="form-group">
                                                        <label for="title">Назва організації</label>
                                                        <input name="title" type="text" class="form-control"
                                                               placeholder="Назва організації">
                                                    </div>

                                                    <div class="radio">
                                                        <label><input type="radio" name="show" value="hide" checked>
                                                            Hide</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="show" value="show">
                                                            Show</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address">Адреса</label>
                                                        <input name="address" type="text" class="form-control"
                                                               placeholder="Адреса">
                                                        <label for="tel">Телефон</label>
                                                        <input name="tel" type="text" class="form-control"
                                                               placeholder="Телефон">
                                                    </div>


                                                    <?php
                                                    if (!empty($_SESSION)) {
                                                        if (isset($_SESSION['postMessage'])) {
                                                            echo $_SESSION['postMessage'];
                                                            unset($_SESSION['postMessage']);
                                                        }

                                                        if (isset($_SESSION['postErrorMessage'])) {
                                                            echo $_SESSION['postErrorMessage'];
                                                            unset($_SESSION['postErrorMessage']);
                                                        }
                                                    }
                                                    ?>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal"> Close
                                                        </button>
                                                        <button name="sendContact" type="submit" class="btn btn-primary">
                                                            Add contact
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                ?>
                            </div>
                        </div>
                        <?php

                        foreach ($allPosts as $post) {
                            if (empty($post['img'])) {
                                $post['img'] = "assets/uploads/default.jpg";
                            }
                            ?>

                            <div class="row row_post">

                                <div class="col-xs-12">
                                    <h4 >
                                        <a href="view/postAdmin.php?post_id=<?= $post['id'] ?>"><?= $post['title'] ?></a>
                                    </h4>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <a href="view/postAdmin.php?post_id=<?= $post['id'] ?>" class="thumbnail"><img
                                                src=<?= $post['img'] ?> alt=""> </a>
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <span><a href="adminParser.php?post_category=<?= $post['category'] ?>">Категорія: <?= $post['category'] ?></a>  </span>
                                    <p>    <?= $post['excerpt'] ?>  </p>

                                    <div class="row">
                                        <div class="col-xs-3 col-sm-2">
                                            <?php


                                            if ($post['show'] == 'show') {
                                                ?>
                                                <form class="input_radio">
                                                    <div class="radio">
                                                        <label><input type="radio" name="<?= $post['id'] ?>"
                                                                      value="hide"> Hide</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="<?= $post['id'] ?>"
                                                                      value="show" checked> Show</label>
                                                    </div>

                                                </form>
                                                <div class="result"></div>


                                                <?php
                                            } else {

                                                ?>
                                                <form class="input_radio">
                                                    <div class="radio">
                                                        <label><input type="radio" name="<?= $post['id'] ?>"
                                                                      value="hide" checked> Hide</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="<?= $post['id'] ?>"
                                                                      value="show"> Show</label>
                                                    </div>

                                                </form>
                                                <div class="result"></div>


                                                <?php
                                            }
                                            ?>

                                        </div>

                                        <div class="col-xs-2 col-sm-2">
                                            <a href="view/postAdmin.php?post_id=<?= $post['id'] ?>">
                                                <button type="button" class="btn btn " data-toggle="modal">Full</button>
                                            </a>
                                        </div>


                                        <div class="col-xs-2 col-sm-2">
                                            <button type="button" class="btn btn " data-toggle="modal"
                                                    data-target="#editPost<?= $post['id'] ?>"
                                                    value="<?= $post['id'] ?>">
                                                Edit
                                            </button>
                                            <!-- Modal Edit -->
                                            <div class="modal fade" id="editPost<?= $post['id'] ?>" tabindex="-1"
                                                 role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Edit post</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form action="" method="post"
                                                                  enctype="multipart/form-data">
                                                                <div class="form-group">
                                                                    <label for="title">Title</label>
                                                                    <textarea name="editTitle" rows="3"
                                                                              cols="60"><?= $post['title'] ?></textarea>
                                                                </div>
                                                                <select class="select" name="editCategory">
                                                                    <option value="Правова допомога">Правова допомога
                                                                    </option>
                                                                    <option value="Психологічна підтримка">Психологічна
                                                                        підтримка
                                                                    </option>
                                                                    <option value="Медична допомога">Медична допомога
                                                                    </option>
                                                                    <option value="Психологічна підтримка">Соціальна допомога
                                                                    </option>
                                                                    <option value="Контакти">Контакти
                                                                    </option>
                                                                    <option value="Інша">Інша</option>
                                                                </select>
                                                                <div class="radio">
                                                                    <label><input type="radio" name="editShow"
                                                                                  value="hide" checked> Hide</label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label><input type="radio" name="editShow"
                                                                                  value="show"> Show</label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="excerpt">Excerpt</label>
                                                                    <textarea name="editExcerpt" rows="6"
                                                                              cols="75"><?= $post['excerpt'] ?> </textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="fullPost">EditFullPost</label>
                                                                    <textarea name="editFullPost"
                                                                              id="fullPost<?= $post['id'] ?>" rows="20"
                                                                              cols="60">
                                                                       <?= $post['excerpt'] ?>
                                                                    </textarea>
                                                                    <script type="text/javascript">
                                                                        CKEDITOR.replace("fullPost<?= $post['id']?>");
                                                                    </script>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="title">Link</label>
                                                                    <textarea name="editLink" rows="2" cols="60"
                                                                              disabled><?= $post['afullpost'] ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label id="dbImg">Image in DB: </label>
                                                                    <textarea name="imgDb" rows="1"
                                                                              cols="60"><?= $post['img'] ?></textarea>
                                                                    <label for="img">Select another image to
                                                                        upload:</label>
                                                                    <input type="file" name="editFileToUpload"
                                                                           id="editFileToUpload">
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button name="editPostSend" type="submit"
                                                                            class="btn btn-primary"
                                                                            value="<?= $post['id'] ?>">Edit post
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-2">
                                            <form action="" method="post">
                                                <div class="form-group">
                                                    <button name="deletePost" type="submit" class="btn btn"
                                                            value="<?= $post['id'] ?>">Delete
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <ul class="list-inline">
                                            <li><i class="fa fa-user" aria-hidden="true"></i> by <a
                                                        href="<?= $post['afullpost'] ?>"
                                                        target="_blank"><?= $post['author'] ?></a>
                                                |
                                            </li>
                                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $post['date'] ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="pagination">
                                    <?php if (($postsCount > $count) and ($allPages == true)) {
                                        for ($i = 0; $i < $len; $i++) { ?>

                                            <li><a href="?page=<?= $i + 1 ?>"><?= $i + 1 ?></a></li>

                                        <?php }
                                    }
                                    if (($postsCategoryCount > $count) and ($allPages == false)) {
                                        for ($i = 0; $i < $lenByCategory; $i++) { ?>
                                            <li>
                                                <a href="?page=<?= $i + 1 ?>&post_category=<?= $postCategory ?>"><?= $i + 1 ?></a>
                                            </li>
                                        <?php }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                }
            }
            ?>
        </div>
        </div>
    </main>
    <script>
        $(document).ready(function () {
            $('input[type=radio]').change(function () {
                var id=this.name;
                var show=this.value;

                $.ajax({
                    url:"adminParser.php",
                    method:"POST",
                    data:{id:id, show:show },
                    success:function () {
                      //  $('.result').html(data);
                    }
                })
            });
        });
    </script>

<?php
require_once "view/footer.php";
?>