<?php
$ref = $_SERVER['PHP_SELF'];

if (($ref == "/view/post.php") || ($ref == "/view/postAdmin.php") || ($ref == "/view/editPost.php")){
    require_once "../model/blogManager.php";
}
else{
    require_once "model/blogManager.php";
}

session_start();

class blogController{
    public function __construct(){
    }

    public function addPost($title, $category, $excerpt, $fullPost,$author, $img ,$show ){
        $this->$title=$title;
        $this->$category=$category;
        $this->$excerpt=$excerpt;
       $this->$fullPost=$fullPost;
        $this->$author=$author;
        $this->$img=$img;
        $this->$show=$show;


        date_default_timezone_set('Europe/Kiev');
        $date = new \DateTime();
        $date->getTimestamp();
        $postTime=$date->format('Y-m-d H:i:s');

            $db= new blogManager();

            $postResult = $db->addNewPost( $title, $category, $excerpt, $fullPost, $author, $postTime, $img, $show);
            if($postResult){
                $postMessage="<p> Post successfully added!</p>";
                $_SESSION['postMessage']=$postMessage;
            }
            else {
                $postErrorMessage = "<p>Post not added!</p>";
                $_SESSION['postErrorMessage'] = $postErrorMessage;
            }
      }

    public function addParsePost($title, $category, $excerpt, $fullPost,$author, $img, $afullpost, $show){
        $this->$title=$title;
        $this->$category=$category;
        $this->$excerpt=$excerpt;
        $this->$fullPost=$fullPost;
        $this->$author=$author;
        $this->$img=$img;
        $this->$afullpost=$afullpost;
        $this->$show=$show;

        date_default_timezone_set('Europe/Kiev');
        $date = new \DateTime();
        $date->getTimestamp();
        $postTime=$date->format('Y-m-d H:i:s');

        $db= new blogManager();
        $postResult = $db->addNewParsePost( $title, $category, $excerpt, $fullPost,$author, $postTime, $img, $afullpost, $show);
       // print_r($postResult);
        return $postResult;
    }

    public function checkParsePost($afullpost){
        $this->$afullpost=$afullpost;
        $db=new blogManager();
        $checkResult = $db->checkPost($afullpost);
        $checkResult= mysqli_fetch_assoc($checkResult);
        return $checkResult;
    }

    public function editPostF($id, $editTitle, $editCategory, $editExcerpt, $editFullPost, $editTarget_file, $editShow){
        $this->$editTitle=$editTitle;
        $this->$editCategory=$editCategory;
        $this->$editExcerpt=$editExcerpt;
        $this->$editFullPost=$editFullPost;
        $this->$editTarget_file=$editTarget_file;
        $this->$editFullPost=$editFullPost;
        $this->$editShow=$editShow;
        $this->$id=$id;
        $db= new blogManager();
        $editPostResult = $db->editPost($id, $editTitle, $editCategory, $editExcerpt, $editFullPost, $editTarget_file, $editShow);
        return $editPostResult;
    }

    public function countPosts(){
        $db= new blogManager();
        $result=$db->countAllPosts();
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function countShowPosts($show){
        $show='show';
        $db= new blogManager();
        $result=$db->countShowPosts($show);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function countPostsByAuthor($author){
        $this->$author=$author;
        $db= new blogManager();
        $result=$db->countAllPostsByAuthor($author);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function writePosts($start, $count){
          $this->$start=$start;
          $this->$count=$count;
          $db= new blogManager();
          $result=$db->writeAllPosts($start, $count);
           $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
          return $result;
      }

    public function writeShowPosts($start, $count){
        $this->$start=$start;
        $this->$count=$count;
        $db= new blogManager();
        $result=$db->writeShowPosts($start, $count);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }
      
    public function getPost($postId){
        $this->$postId = $postId;
        $db = new blogManager();
        $result = $db->getFullPost($postId);
        $result = mysqli_fetch_assoc($result);
        return $result;
    }

    public function delPost($delPost){
        $this->$delPost=$delPost;
        $db=new blogManager();
        $result = $db->delPost($delPost);
        if($result){
            $_SESSION['postDelMessage']="Post successfully deleted";
        }
        else{
            $_SESSION['postErDelMessage']="Post not deleted";
        }
    }

    public function sortPostAuthor($author,$start, $count){
        $this->$author=$author;
        $this->$start=$start;
        $this->$count=$count;
        $db=new blogManager();
        $result = $db->sortPostByAuthor($author,$start, $count);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function countPostsByCategory($category){
        $this->$category=$category;
        $db= new blogManager();
        $result=$db->countAllPostsByCategory($category);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function countShowPostsByCategory($category, $show){
        $this->$category=$category;
        $this->$show=$show;
        $db= new blogManager();
        $result=$db->countShowPostsByCategory($category, $show);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function sortPostCategory($category,$start, $count){
        $this->$category=$category;
        $this->$start=$start;
        $this->$count=$count;
        $db=new blogManager();
        $result = $db->sortPostByCategory($category,$start, $count);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function sortShowPostCategory($category, $show,$start, $count){
        $this->$category=$category;
        $this->$start=$start;
        $this->$count=$count;
        $this->$show=$show;
        $db=new blogManager();
        $result = $db->sortShowPostByCategory($category, $show,$start, $count);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function changeShow($id, $show){
        $this->$show=$show;
        $this->$id=$id;
        $db=new blogManager();
        $result = $db->changeShow($id, $show);
    }

    public function parse($parseName){
        $this->$parseName=$parseName;

        date_default_timezone_set('Europe/Kiev');
        $date = new \DateTime();
        $date->getTimestamp();
        $parseTime=$date->format('Y-m-d H:i:s');

        $db=new blogManager();
        $checkRes=$db-> checkParse($parseName);
        $checkRes= mysqli_fetch_assoc($checkRes);
        $checkResName=$checkRes['parseName'];
        if($checkResName==$parseName){
            echo "<pre>";
            print_r($checkRes);
            echo "</pre>";
            $updateParse=$db->updateParser ($parseName, $parseTime);
            return $updateParse;
        }
        else {
            echo "2"."<br>";
            echo "<pre>";
        print_r($checkRes);
        echo "</pre>";
            $resultParse=$db->parse($parseName, $parseTime);
        return $resultParse;
        }
    }

    public function getAllParseTime(){

        $db = new blogManager();
        $result = $db->checkAllParses();
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function addContact($title, $address, $tel, $show ){
        $this->$title=$title;
        $this->$address=$address;
        $this->$tel=$tel;
        $this->$show=$show;

        $db= new blogManager();

        $addResult = $db->addNewContact( $title, $address, $tel, $show);

    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }


}