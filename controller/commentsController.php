<?php
if (($ref == "/view/post.php") || ($ref =="/controller/commentsController.php") ||($ref == "/view/postAdmin.php")){
    require_once "../model/commentsManager.php";
}
else {
    require_once "model/commentsManager.php";
}
session_start();

class commentsController{

    public function __construct(){
    }

    public function checkCommUser($login){
        $this->$login=$login;
//        $this->$email=$email;
        $db=new commentsManager();
        $checkResult = $db->checkCommUser($login);
        $checkResult= mysqli_fetch_assoc($checkResult);
        return $checkResult;
    }
    public function checkCommUserByL($login){
        $this->$login=$login;
        $db=new commentsManager();
        $checkResult = $db->checkCommUserByL($login);
        $checkResult= mysqli_fetch_assoc($checkResult);
        return $checkResult;
    }
    public function checkCommUserByM($email){
        $this->$email=$email;
        $db=new commentsManager();
        $checkResult = $db->checkCommUserByM($email);
        $checkResult= mysqli_fetch_assoc($checkResult);
        return $checkResult;
    }


    public function addNewCommU($newUser,$email, $password){
        $this->$newUser=$newUser;
        $this->$password=$password;
        $this->$email=$email;
        //validation
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
            echo $emailErr;
        }
        else{
            if((mb_strlen($newUser)>=3 && mb_strlen($newUser)<=16) && (mb_strlen($password)>=6 && mb_strlen($password)<=32)){
                $this->$password=hash('sha512', $password);
                $db= new commentsManager();
                $newUserResult = $db->addNewCommUser( $this->$newUser, $this->$email, $this->$password);
                if($newUserResult==1){
                    $userMessage="<p> User successfully added!</p>";
                    $_SESSION['userMessage']=$userMessage;
                    $_SESSION['login']=$_SESSION[$newUser];
                }
                else if($newUserResult == 0){
                    $userErrorMessage = "<p>User not added!</p>";
                    $_SESSION['userErrorMessage'] = $userErrorMessage;
                }
            }
            else{
                $loginError ="Incorrect login or password. Login must be between 3 and 16 characters in length, password 6-32.";
                $_SESSION['loginError']=$loginError;
            }
        }

    }


    public function addComment($author, $commentsText, $postId){
        $this->$author=$author;
        $this->$commentsText=$commentsText;
        $this->$postId=$postId;

        $datetimeFormat = 'Y-m-d H:i:s';
        date_default_timezone_set('Europe/Kiev');
        $date = new \DateTime();
        $date->getTimestamp();
        $commTime=$date->format($datetimeFormat);

        $db= new commentsManager();

        $commResult = $db->addNewComment( $author, $commentsText, $commTime, $postId);
        if($commResult){
            $commMessage="<p> Comment successfully added!</p>";
            $_SESSION['commMessage']=$commMessage;
        }
        else {
            $commErrorMessage = "<p>Comment not added!</p>";
            $_SESSION['commErrorMessage'] = $commErrorMessage;
        }
    }
    public function getComments($postId){
        $this->$postId=$postId;
        $db=new commentsManager();
        $result=$db->getCommentsById($postId);
        $result= mysqli_fetch_all($result,MYSQLI_ASSOC );
        return $result;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}