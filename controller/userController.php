<?php

session_start();
require_once "model/dbManager.php";

class userController{

    public function __construct(){
    }
    //приймаємо дані
    public function addNewU($newUser, $password, $role){
        $this->$newUser=$newUser;
        $this->$password=$password;
        $this->$role=$role;
        //validation
        if((mb_strlen($newUser)>=3 && mb_strlen($newUser)<=16) && (mb_strlen($password)>=6 && mb_strlen($password)<=32)){
            $this->$password = hash('sha512', $password);


            $db= new dbManager();
            $newUserResult = $db->addNewUser( $this->$newUser, $this->$password, $this->$role);
        }
        else{
            $loginError ="Incorrect login or password. Login must be between 3 and 16 characters in length, password 6-32.";
            $_SESSION['loginError']=$loginError;
        }
    }
    public function checkUser($login){
        $this->$login=$login;
        $db=new dbManager();
        $checkResult = $db->getUser($login);
        $checkResult= mysqli_fetch_assoc($checkResult);
        return $checkResult;
    }

    public function getAllUsers(){
        $db=new dbManager();
        $checkResult = $db->getAllUsers();
        $checkResult= mysqli_fetch_all($checkResult,MYSQLI_ASSOC );
        return $checkResult;
    }

    public function editUser($editLogin, $editRole)  {
        $this->$editLogin = $editLogin;
        $this->$editRole = $editRole;
        if ($editLogin == 'admin') {
            $this->$editRole = 'admin';
            $adminError = "<p>You can't change role for admin </p>";
            $_SESSION['$adminRole'] = $adminError;
        }
        else {
            $db = new dbManager();
            $changResult = $db->editNewUser($editLogin, $editRole);
        }
    }

    public function delete($delLogin){
        $this->$delLogin=$delLogin;
        $db=new dbManager();
        $deleteResult = $db->deleteUser($this->$delLogin);
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}