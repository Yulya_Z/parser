<?php
session_start();


if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}
$_SESSION['init'] = true;
require_once "controller/blogController.php";

if (isset($_POST['authComUser']) and isset($_POST['commUser']) and isset($_POST['commPassword'])) {
    $commUser = trim($_POST['commUser']);
    $commPassword = trim($_POST['commPassword']);
    $commPassword =  hash('sha512', $commPassword);

    require_once "controller/commentsController.php";
    $authUser = new commentsController();
    $authResult = $authUser->checkCommUserByL($commUser);
    $loggedCommUser = $authResult['login'];
    $loggedCommPass = $authResult['password'];

    if ($commUser == $loggedCommUser and $commPassword == $loggedCommPass) {
        $_SESSION['login'] = $loggedCommUser;
        $_SESSION['logged'] = true;
    }
}
else if (isset($_POST['authComUser']) and isset($_POST['commUserEmail']) and isset($_POST['commPassword'])) {
    $commUserEmail = trim($_POST['commUserEmail']);
    $commPassword = trim($_POST['commPassword']);
    $commPassword =  hash('sha512', $commPassword);

    require_once "controller/commentsController.php";
    $authUser = new commentsController();
    $authResult = $authUser->checkCommUserByM($commUserEmail);
    $loggedCommUser = $authResult['login'];
    $loggedCommUserEmail = $authResult['email'];
    $loggedCommPass = $authResult['password'];

    if ($commUserEmail == $loggedCommUserEmail and $commPassword == $loggedCommPass) {
        $_SESSION['login'] = $loggedCommUser;
        $_SESSION['logged'] = true;
    }
}
if (isset($_POST['addCommUser'])) {
    if (isset($_POST['commUser']) and isset($_POST['commUserEmail']) and isset($_POST['commPassword']) and isset($_POST['re-commPassword'])) {
        $newCommUser = trim($_POST['commUser']);
        $commUserEmail = trim($_POST['commUserEmail']);
        $commPassword = trim($_POST['commPassword']);
        $reCommPassword = trim($_POST['re-commPassword']);


        if ($commPassword == $reCommPassword) {
            require_once "controller/commentsController.php";
            $putUser = new commentsController();
            $checkedUser = $putUser->checkCommUser($newCommUser);
            $checkedUser = $checkedUser['commUsername'];
            if ($checkedUser == $newCommUser) {
                $_SESSION['checkedUser'] = "<p>This user exist!</p>";
            } else {
                $putUser->addNewCommU($newCommUser, $commUserEmail, $commPassword);
            }
        } else {
            echo "Wrong password";
        }
    }
}

require_once "view/header.php";
?>
    <main>
        <div class="container-fluid">
             <div class="container">
            <?php
            $count = 10;
            $allPages=true;
            $post = new blogController();
            $show='show';
            $allPosts = $post->writeShowPosts($start=0, $count);

            $countShowPosts = $post->countShowPosts($show);
            $postsCount = count($countShowPosts);
            $len = ceil( $postsCount / $count);

            if ((isset($_GET['page']))){
                $page = $_GET['page'];
                for($page==1; $page<=$len; $page++) {
                    $start = ($page - 1) * $count;
                    $allPosts = $post->writeShowPosts($start, $count);
                    break;
                }
            }

            if(!empty($_GET['post_category'])){
                $allPages=false;
                $postCategory=$_GET['post_category'];
                $show='show';
                $allPosts = $post->sortShowPostCategory($postCategory, $show,$start=0, $count);
                $countCategoryPosts = $post->countShowPostsByCategory($postCategory, $show);
                $postsCategoryCount = count($countCategoryPosts);
                $lenByCategory = ceil( $postsCategoryCount / $count);

                if (isset($_GET['page']) && !empty($_GET['page']) AND isset($_GET['post_category']) && !empty($_GET['post_category'])){
                    for($page==1; $page<=$len; $page++) {
                        $start = ($page - 1) * $count;
                        echo $start."<br>";
                        $allPosts = $post->sortPostCategory($postCategory,$start, $count);
                        break;
                    }
                }
            }


            foreach ($allPosts as $post) {
                if(empty($post['img'])){
                    $post['img']="assets/uploads/default.jpg";
                }

                ?>
                <div class="row row_post">
                    <div class="col-xs-12 ">
                        <h4 class="text-center"><a href="view/post.php?post_id=<?=$post['id']?>"><?= $post['title']?></a></h4>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="view/post.php?post_id=<?=$post['id']?>" class="thumbnail"><img src=<?=$post['img']?> alt=""> </a>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <span> <a href="index.php?post_category=<?= $post['category']?>">Категорія: <?= $post['category'] ?></a>  </span>
                        <p>
                            <?= $post['excerpt'] ?>
                        </p>

                        <p><a class="btn btn-default" href="view/post.php?post_id=<?=$post['id']?>">Read more</a></p>
                        <br/>

                        <ul class="list-inline">
                            <li><i class="fa fa-user" aria-hidden="true"></i> by <a href="<?=$post['afullpost']?>" target="_blank"><?= $post['author'] ?></a> |</li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $post['date'] ?></li>
                        </ul>
                    </div>
                </div>
                <?php

            }
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="pagination">
                        <?php if(($postsCount> $count)and($allPages==true)){
                         for ($i = 0; $i < $len; $i++) { ?>
                            <li><a href="?page=<?= $i + 1 ?>"><?= $i + 1 ?></a></li>
                        <?php }
                        }
                        if(($postsCategoryCount> $count)and($allPages==false)){
                            for ($i = 0; $i < $lenByCategory; $i++) { ?>
                                <li><a href="?page=<?= $i + 1?>&post_category=<?=$postCategory ?>"><?= $i + 1 ?></a></li>
                            <?php }
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
        </div>
    </main>


<?php
require_once "view/footer.php";
?>