<?php
require_once "config.php";
class blogManager{
    private $mysqli;
    public function __construct()  {
        $this->mysqli = new mysqli(HOST, dbUser, dbUserPass, dbName) or die("Unable to connect");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function addNewPost( $title, $category, $excerpt, $fullPost,$author, $postTime, $img, $show ){
        $result =  $this->mysqli->query("INSERT INTO parsePost (`id`, `author`, `title`, `category`, `excerpt`, `fullpost`, `date`,`img`, `show`) VALUES (NULL, '$author','$title','$category', '$excerpt', '$fullPost', '$postTime','$img', '$show'  )");
        return $result;
    }
    public function addNewParsePost( $title, $category, $excerpt,  $fullPost, $author, $postTime, $img, $afullpost, $show){
         $fullPost = mysqli_real_escape_string($this->mysqli, $fullPost);
        $result =  $this->mysqli->query("INSERT INTO parsePost (`id`, `author`, `title`, `category`, `excerpt`, `fullpost`, `date`, `img`, `afullpost`, `show`) VALUES (NULL , '$author','$title','$category', '$excerpt', '$fullPost', '$postTime','$img','$afullpost', '$show' )");
        return $result;
    }

    public function checkPost($afullpost){
        $result=$this->mysqli->query("SELECT * FROM `parsePost` WHERE `afullpost`= '$afullpost'");
        return $result;
    }

    public function editPost($id, $editTitle, $editCategory, $editExcerpt, $editFullPost, $editTarget_file, $editShow ){
        $editFullPost = mysqli_real_escape_string($this->mysqli, $editFullPost);
        $result =  $this->mysqli->query("UPDATE `parsePost`  SET `title`='$editTitle', `category`='$editCategory', `excerpt`='$editExcerpt', `fullpost`='$editFullPost',`img`='$editTarget_file', `show`='$editShow'  WHERE `id`='$id' ");
        return $result ;
    }

    public function countAllPosts(){
        $result =  $this->mysqli->query("SELECT * FROM `parsePost`");
        return $result;
    }
    public function writeShowPosts($start, $count){
        $result =  $this->mysqli->query("SELECT `id`, `author`, `title`,`category`, `excerpt`, `date`,`img`, `afullpost` FROM `parsePost`  WHERE `show`='show' ORDER BY `date` DESC LIMIT $start, $count");
        return $result;
    }
    public function writeAllPosts($start, $count){
        $result =  $this->mysqli->query("SELECT * FROM `parsePost` ORDER BY `date` DESC LIMIT $start, $count");
        return $result;
    }

    public function getFullPost($postId){
        $result = $this->mysqli->query("SELECT * FROM `parsePost` WHERE `id` = '$postId'");
        return $result;
    }

    public function delPost($delPost){
        $result = $this->mysqli->query("DELETE FROM `parsePost` WHERE `id`= '$delPost'");
        return $result;
    }

    public function sortPostByCategory($categoryP,$start, $count){
        $result =  $this->mysqli->query("SELECT `id`, `author`, `title`,`category`, `excerpt`, `date`,`img` FROM `parsePost` WHERE `category` = '$categoryP' ORDER BY `category`,`date`  DESC LIMIT $start, $count ");
        return $result;
    }

    public function sortShowPostByCategory($categoryP, $show, $start, $count){
        $result =  $this->mysqli->query("SELECT `id`, `author`, `title`,`category`, `excerpt`, `date`,`img` FROM `parsePost` WHERE (`category` = '$categoryP')AND (`show`='$show') ORDER BY `category`,`date`  DESC LIMIT $start, $count ");
        return $result;
    }

    public function countAllPostsByCategory($categoryP){
        $result =  $this->mysqli->query("SELECT * FROM `parsePost`  WHERE `category` = '$categoryP'");
        return $result;
    }

    public function countShowPostsByCategory($categoryP , $show){
        $result =  $this->mysqli->query("SELECT * FROM `parsePost`  WHERE (`category` = '$categoryP') AND (`show`='$show')");
        return $result;
    }

    public function countShowPosts($show){
        $result =  $this->mysqli->query("SELECT * FROM `parsePost`  WHERE `show` = '$show'");
        return $result;
    }
    public function changeShow($id, $show){
        $result =  $this->mysqli->query("UPDATE `parsePost`  SET `show`='$show' WHERE `id` = '$id' ");
        return $result;
    }

    public function parse( $parseName,$parseTime ){
        $result =  $this->mysqli->query("INSERT INTO `parse` (`id`, `parseName`, `parseDate`) VALUES (NULL, '$parseName','$parseTime') ");
        return $result;
    }

    public function checkParse($parseName){
        $result=$this->mysqli->query("SELECT * FROM  `parse` WHERE `parseName` = '$parseName'");
        return $result;
    }
    public function updateParser ($parseName, $parseTime){
        $result =  $this->mysqli->query("UPDATE `parse`  SET `parseDate`='$parseTime'  WHERE `parseName`='$parseName' ");
        return $result ;
    }
    public function checkAllParses(){
        $result=$this->mysqli->query("SELECT `parseName`, `parseDate` FROM  `parse`");
        return $result;
    }
    public function addNewContact( $title, $address, $tel, $show){
        $result =  $this->mysqli->query("INSERT INTO contacts (`id`, `name`, `address`, `tel`, `show_contacts`) VALUES (NULL, '$title', '$address', '$tel', '$show')");
        return $result;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}