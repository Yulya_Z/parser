<?php

require_once "config.php";

class commentsManager{

    private $mysqli;

    public function __construct() {
        $this->mysqli = new mysqli(HOST, dbUser, dbUserPass, dbName) or die("Unable to connect");
        $this->mysqli->query("SET NAMES 'utf8'");
    }
    public function addNewCommUser($login,$email, $password){
        $result =  $this->mysqli->query("INSERT INTO commentUser (`id`, `login`, `email`, `password`) VALUES (NULL, '$login','$email', '$password')");
        return $result;
    }

    public function checkCommUser($login){
        $result=$this->mysqli->query("SELECT * FROM `commentUser` WHERE `login`= '$login' ");
        return $result;
    }
    public function checkCommUserByL($login){
        $result=$this->mysqli->query("SELECT * FROM `commentUser` WHERE `login`= '$login'");
        return $result;
    }

    public function checkCommUserByM($email){
        $result=$this->mysqli->query("SELECT * FROM `commentUser` WHERE `email`= '$email'");
        return $result;
    }

    public function addNewComment( $login,$comment, $commTime, $postId){
        $result =  $this->mysqli->query("INSERT INTO comments (`id`, `author`, `comment`, `date`, `postId`) VALUES (NULL, '$login','$comment', '$commTime', '$postId')");
        return $result;
    }

    public function getCommentsById($postId){
        $result =  $this->mysqli->query("SELECT `id`,`author`,`comment`,`date` FROM `comments`  WHERE `postId` = '$postId' ORDER BY `date` DESC");
        return $result;
    }

    public function __destruct() {
        if ($this->mysqli) {
            $this->mysqli->close();
        }
    }
}