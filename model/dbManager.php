<?php
require_once "config.php";
class dbManager{
    private $mysqli;

    public function __construct()
    {
       $this->mysqli = new mysqli(HOST, dbUser, dbUserPass, dbName) or die("Unable to connect");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function addNewUser($login, $password, $role){
        $result =  $this->mysqli->query("INSERT INTO blogUsers (id, username, password, role) VALUES (NULL, '$login', '$password', '$role')");
        return $result;
    }

    public function getUser($login){
        $result=$this->mysqli->query("SELECT * FROM `blogUsers` WHERE `username`= '$login'");
        return $result;
    }

    public function getAllUsers(){
        $result=$this->mysqli->query("SELECT * FROM `blogUsers` ");
        return $result;
    }

    public function deleteUser($delLogin){
        $result=$this->mysqli->query("DELETE FROM `blogUsers` WHERE `username`= '$delLogin'");
        return $result;
    }

    public function editNewUser($editLogin, $editRole){
        $result=$this->mysqli->query("UPDATE `blogUsers` SET `role`='$editRole' WHERE `username`='$editLogin'");
        // var_dump($result);
        return $result;
    }


    public function __destruct()
    {
        if($this->mysqli){
            $this->mysqli->close();
        }
    }

}