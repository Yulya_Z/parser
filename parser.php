<?php
//session_start();
require_once 'phpQuery.php';
require_once 'controller/blogController.php';
require_once "view/header.php";

if (!empty($_POST)) {

    if (isset($_POST['submitName'])) {
        $parseName = $_POST['submitName'];
        $parse = new blogController();
        $parseSomeName = $parse->parse($parseName);

        if ($parseName == "BezbroniLegal") {
            parseBezbroni('http://bezbroni.net/information?type=0', 0, 2);
        } else if ($parseName == "BezbroniPsychological") {
            parseBezbroni('http://bezbroni.net/information?type=1', 0, 2);
        } else if ($parseName == "Kagarluk") {
            parseKagarlukArticle('http://www.kagarlyk-rda.gov.ua/index.php/pravova-dopomoga-meshkantsyam-kijivshchini/pravova-dopomoga/1961-yaki-pilgi-mozhe-otrimati-uchasnik-ato');
        } else if ($parseName == "DomivkaPravova") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D0%B2%D0%B0-%D0%B4%D0%BE%D0%BF%D0%BE%D0%BC%D0%BE%D0%B3%D0%B0/page/2/', 0, 1);
        } else if ($parseName == "DomivkaKontaktu") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B8/', 0, 1);
        } else if ($parseName == "DomivkaMed") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BC%D0%B5%D0%B4%D0%B8%D1%87%D0%BD%D0%B0-%D0%B4%D0%BE%D0%BF%D0%BE%D0%BC%D0%BE%D0%B3%D0%B0/', 0, 1);
        } else if ($parseName == "DomivkaPravova2") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/uncategorized/', 0, 1);
        } else if ($parseName == "DomivkaUBD") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BE%D1%82%D1%80%D0%B8%D0%BC%D0%B0%D0%BD%D0%BD%D1%8F-%D1%83%D0%B1%D0%B4/', 0, 1);
        } else if ($parseName == "DomivkaPsychologicalHelp") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BF%D1%81%D0%B8%D1%85%D0%BE%D0%BB%D0%BE%D0%B3%D1%96%D1%87%D0%BD%D0%B0-%D0%B4%D0%BE%D0%BF%D0%BE%D0%BC%D0%BE%D0%B3%D0%B0/', 0, 1);
        } else if ($parseName == "DomivkaSocialHelp") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D1%81%D0%BE%D1%86%D1%96%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0-%D0%B4%D0%BE%D0%BF%D0%BE%D0%BC%D0%BE%D0%B3%D0%B0/', 0, 1);
        } else if ($parseName == "DomivkaNews") {
            parseDomivka('http://www.dopomoga-cv.gov.ua/category/%D0%BD%D0%BE%D0%B2%D0%B8%D0%BD%D0%B8/page/2/', 0, 10);
        }
        else if( $parseName=="BezbroniKontaktu"){
            parseBezbroniKontaktu('http://bezbroni.net/?OrganizationSearch%5Bregion%5D=%D0%A0%D1%96%D0%B2%D0%BD%D0%B5%D0%BD%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C&OrganizationSearch%5Bid_type%5D=', 0, 3);
        }
    }
}

//------- PARSER ---------
//---------CURL------------
function get_result($url){
    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $res=curl_exec($ch);
    return $res;
}

//---------- Bezbroni--------------
function parseBezbroni($url, $start, $end){
    if($start < $end) {
        $html = phpQuery::newDocument(get_result($url));
        foreach ($html->find('.news-list-item') as $elem) {
            $el = pq($elem);
            $aHref = $el->find('a')->attr('href');
            $aArticle = "http://bezbroni.net" . $aHref;
            $excerpt = $el->find('p')->html();
            $img = $el->find('.news-img img')->attr('src');
            if(stristr($url, 'type=0') ) {
                $category='Правова допомога';
            }
            else if (stristr($url,'type=1')){
                $category='Психологічна підтримка';
            }
            else{
                $category='Інша';
            }
            parseBezbroniArticle($aArticle, $excerpt, $category);
        }
        $newNext=$html->find('.pagination .next a')->attr('href');
        $newNextUrl="http://bezbroni.net".$newNext;
        if (!empty($newNext)) {
            $start++;
            parseBezbroni($newNextUrl, $start, $end);
        }
    }
}

function parseBezbroniArticle($url, $excerpt, $category){
    $html=phpQuery::newDocument(get_result($url));
    $elArticle=pq($html);
    $title=$elArticle->find('.article .page-title a')->text();
    $articleImg=$elArticle->find('.article-body--image img')->attr('src');
    $imgName=substr($articleImg, 20);
    $articleImgSrc='http://bezbroni.net'.$articleImg;
    $fullPost=$elArticle->find('.article-body')->html();
    file_put_contents("assets/uploads/".$imgName, file_get_contents($articleImgSrc));

    $afullpost=$url;
    $author='http://bezbroni.net';
    $img="assets/uploads".$imgName;
    $show='hide';

    $addPost = new blogController();
    $checkedPost=$addPost->checkParsePost($afullpost);
    $checkedPost = $checkedPost['afullpost'];
    if ($checkedPost == $afullpost) {
        echo "<p>This post exist!</p>";
    }
    else if ($checkedPost != $afullpost) {
        $addPost->addParsePost($title, $category, $excerpt, $fullPost,$author, $img, $afullpost, $show);
    }
}


function parseBezbroniKontaktu($url, $start, $end){
    if($start < $end) {
        $html = phpQuery::newDocument(get_result($url));
        foreach ($html->find('.company-item') as $elem) {
            $el = pq($elem);
            $title=$el->find('.company-item-name')->text();
            $aHref = $el->find('.company-item-name a')->attr('href');
            $aArticle = "http://bezbroni.net". $aHref;

            parseBezbroniKontaktuArticle($aArticle, $title);
        }
        $newNext=$html->find('.pagination .next a')->attr('href');
        $newNextUrl="http://bezbroni.net".$newNext;
        if (!empty($newNext)) {
            $start++;
            parseBezbroniKontaktu($newNextUrl, $start, $end);
        }
    }
}
function parseBezbroniKontaktuArticle($afullpost, $title){
    $html=phpQuery::newDocument(get_result($afullpost));
    $elArticle=pq($html);
    $articleImg=$elArticle->find('.organization-image img')->attr('src');
    $fullPost=$elArticle->find('.organization-info ')->html();
    $excerpt=$elArticle->find('.organization-info h3')->text();
    $imgName=substr($articleImg, 40);
    if(!empty($imgName)) {
        file_put_contents("assets/uploads/" . $imgName, file_get_contents($articleImg));
        $img="assets/uploads".$imgName;
    }
    else {
        $img="assets/uploads/default.jpg";
    }
    $show='hide';
    $category='Контакти';
    $author='http://bezbroni.net';
    $addPost = new blogController();
    $checkedPost=$addPost->checkParsePost($afullpost);
    $checkedPost = $checkedPost['afullpost'];
    if ($checkedPost == $afullpost) {
        echo "<p>This post exist!</p>";
    }
    else if ($checkedPost != $afullpost) {
        $addPost->addParsePost($title, $category, $excerpt, $fullPost,$author, $img, $afullpost, $show);
    }
}



//-----------------------------------

//------- Kagarluk  yaki-pilgi-mozhe-otrimati-uchasnik-ato----

function parseKagarlukArticle($url){
    $html=phpQuery::newDocument(get_result($url));
    $elArticle=pq($html);
    $title=$elArticle->find('#main h2' )->html();
    $excerpt=$elArticle->find('#main .item-page p:first');
    $post=$elArticle->find('#main .item-page');
    $post->find('h2')->remove();
    $post->find('p:last')->remove();
    $fullPost=$post;
    $category='Правова допомога';
    $author='http://www.kagarlyk-rda.gov.ua';
    $img="assets/uploads/default.jpg";
    $afullpost=$url;
    $show='show';

    $addPost = new blogController();
    $checkedPost=$addPost->checkParsePost($afullpost);
    $checkedPost = $checkedPost['afullpost'];
    if ($checkedPost == $afullpost) {
        echo "<p>This post exist!</p>";
    }
    else if ($checkedPost != $afullpost) {
        $addPost->addParsePost($title, $category, $excerpt, $fullPost,$author, $img, $afullpost, $show);
    }
}
$url='http://www.kagarlyk-rda.gov.ua/index.php/pravova-dopomoga-meshkantsyam-kijivshchini/pravova-dopomoga/1961-yaki-pilgi-mozhe-otrimati-uchasnik-ato';
//parseKagarlukArticle($url);

//----------------END  Kagarluk-------------------------------

//-------------Domivka--------------

function parseDomivka($url, $start, $end){
    if($start <= $end) {
    $html = phpQuery::newDocument(get_result($url));
    $category=$html->find('.page-title span')->html();
    foreach ($html->find('.post-content') as $elem) {
        $el = pq($elem);
        $aHref = $el->find('a')->attr('href');
        $title = $el->find('a:first')->html();
        $fullPost = $el->find('.entry-content')->html();
        $excerpt = $fullPost;
        if (strlen($excerpt) > 500) {
            $excerpt = substr($excerpt, 0, 500);
            $excerpt = rtrim($excerpt);
            $excerpt = $excerpt . "...";
        }
        $img = $el->find('.entry-thumbnail img')->attr('src');
        $afullpost=$aHref;
        $author='http://www.dopomoga-cv.gov.ua';
        $img="assets/uploads/default.jpg";
        $show='hide';
        $addPost = new blogController();
        $checkedPost=$addPost->checkParsePost($afullpost);
        $checkedPost = $checkedPost['afullpost'];
        if ($checkedPost == $afullpost) {
            echo "<p>This post exist!</p>";
        }
        else if ($checkedPost != $afullpost) {
            $addPost->addParsePost($title, $category, $excerpt, $fullPost,$author, $img, $afullpost, $show);
        }
    }
    $newNext=$html->find('.paging-navigation .nav-next a')->attr('href');
        if (!empty($newNext)) {
            $start++;
            parseDomivka($newNext, $start, $end);
        }
    }
}

//-----------------

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<main id="admin">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <?php
                if (!empty($_SESSION)) {
                    if (!isset($_SESSION['logged'])) {
                        ?>
                        <div class="col-xs-12">
                            <h2>Authorization</h2>
                            <form action="admin.php" method="post">
                                <div class="form-group">
                                    <label for="login">Login</label>
                                    <input name="auth" type="text" class="form-control" placeholder="Login for new user">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input name="auth-password" type="password" class="form-control" placeholder="Password">
                                </div>
                                <button name="send-auth" type="submit" class="btn btn-default">LOG IN</button>
                            </form>
                            <?php
                            if (!empty($_SESSION)) {
                                if (isset($_SESSION['authError'])) {
                                    echo $_SESSION['authError'];
                                    unset($_SESSION['authError']);
                                }
                            }
                            ?>
                            <hr>
                        </div>
                        <?php
                    }
                }
                ?>

                <?php
                if (!empty($_SESSION)) {
                    if (isset($_SESSION['logged'])) {
                        if (($_SESSION['role'] == 'admin') or ($_SESSION['role'] == 'moderator')) {
                                ?>

                <form action="parser.php" method="post" name="parse" id="parse">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <input type="submit" class="button parse" name="bezbroni1" value="BezbroniLegal"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="bezbroni2" value="BezbroniPsychological"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="Kagarluk" value="Kagarluk"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaPravova" value="DomivkaPravova"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaKontaktu" value="DomivkaKontaktu"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaMedHelp" value="DomivkaMed"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaPravova2" value="DomivkaPravova2"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaUBD" value="DomivkaUBD"/>
                    </div>
                    <div class="col-xs-12  col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaPsychologicalHelp" value="DomivkaPsychologicalHelp"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaSocialHelp" value="DomivkaSocialHelp"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="DomivkaNews" value="DomivkaNews"/>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-md-3">
                        <input type="submit" class="button parse" name="bezbroni3" value="BezbroniKontaktu"/>
                    </div>
                </form>
            </div>
            <div class="row"></div>
                <div class="col-xs-12 timeParse">
                    <p> <b>The name and time of the last parsing: </b></p>
                <?php
                    $parse=new blogController();
                    $parsePosts = $parse->getAllParseTime();
                    foreach ($parsePosts as $parse) {
                        echo($parse['parseName']).":"." ";
                        echo($parse['parseDate'])."<br/>";
                    }
                ?>

                </div>
                <?php
                        }
                    }
                }

                ?>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $('input[type=submit]').click( function () {
            var submitName=this.value;

            $.ajax({
                url:"parser.php",
                method:"POST",
                data:{submitName:submitName },
                success:function () {
                    alert("Function successfully end");
                }
            })
        });
    });

</script>
<?php
require_once "view/footer.php";
?>