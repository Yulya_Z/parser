<?php
session_start();


if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}

if($_POST['btnSearch']){
    $_SESSION['SEARCH']=FormChars($_POST['search']);
    exit(header('location: /search.php'));
}




require_once "view/header.php";
?>
<main>
    <div class="container-fluid">
        <div class="container">
            <form method="post" action="search.php" class="navbar-form ">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Search" required>
                </div>
                <button type="submit"  name="btnSearch" class="btn btn-default">Найти</button>
            </form>

        </div>
    </div>
</main>

<?php
require_once "view/footer.php";
?>