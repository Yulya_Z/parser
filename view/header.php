<?php
session_start();

if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}

$_SESSION['init'] = true;
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Portal </title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" media ="(min-width:480px)" href="../assets/css/style480.css">
    <link rel="stylesheet" media ="(min-width:768px)" href="../assets/css/style768.css">
    <link rel="stylesheet" media ="(min-width:960px)" href="../assets/css/style960.css">
    <link rel="stylesheet" media ="(min-width:1024px)" href="../assets/css/style1024.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <script src="../assets/ckeditor/ckeditor.js"></script>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

</head>

<header>
    <div class="container-fluid">
        <div class="container">
            <section class="top-header">
                <div class="row first">
                    <div class="col-xs-12  text-right">
                        <div class="top-support">
                            <?php
                            if (!isset($_SESSION['logged']) and (!isset($_SESSION['login']))){
                                ?>

                                <a  data-toggle="modal" data-target="#addCommUser" class="login">Register</a>
                                    <div class="modal fade text-left" id="addCommUser" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" >
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">Sign up</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
                                                        <div class="form-group">
                                                            <label for="login">Login</label>
                                                            <input name="commUser" type="text" class="form-control"
                                                                   placeholder="Login of user">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="login">Email</label>
                                                            <input name="commUserEmail" type="text" class="form-control"
                                                                   placeholder="Email">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Password</label>
                                                            <input name="commPassword" type="password"
                                                                   class="form-control" placeholder="Password">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="re-password">Retype-Password</label>
                                                            <input name="re-commPassword" type="password"
                                                                   class="form-control" placeholder="Retype Passwordrd">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button name="addCommUser" type="submit"
                                                                    class="btn btn-success">Sign up
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <a  data-toggle="modal"  data-target="#authComUser" class="login"><b>Login</b></a>
                                <div class="modal fade text-left" id="authComUser" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Enter login or email</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
                                                    <div class="form-group">
                                                        <label for="login">Login</label>
                                                        <input name="commUser" type="text" class="form-control"
                                                               placeholder="Login of user">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input name="commUserEmail" type="text" class="form-control"
                                                               placeholder="Email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Password</label>
                                                        <input name="commPassword" type="password"
                                                               class="form-control" placeholder="Password">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button name="authComUser" type="submit"
                                                                class="btn btn-success">Log in
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                            else if (isset($_SESSION['logged']) and $_SESSION['role'] == 'admin' or $_SESSION['role'] == 'moderator'){
                                echo '<span>Welcome,' . $_SESSION['username'] ."</span>";
                                ?>
                                <a href="../admin.php?exit=exit" class="login">Logout</a></li>
                                <?php
                            }
                            else if(isset($_SESSION['logged'])){
                                echo '<span>Welcome,' . $_SESSION['login'] . "!</span>";
                                ?>
                                <a href="../index.php?exit=exit" class="login">Logout</a></li>

                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="logo">
                            <a href="/index.php"><img src="/assets/uploads/logo.jpg" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <h1>Портал для учасників АТО та УБД</h1>
                    </div>
                    <div class="col-xs-12">

                    <form method="post" action="search.php" class="navbar-form navbar-right">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Найти</button>
                    </form>
                    </div>
                </div>
            </section>
                <div class="row">
                <nav class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="../index.php">Головна</a></li></li>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Допомога <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../news.php">Новини</a></li>
                                    <li><a href="../legal.php">Правова допомога</a></li>
                                    <li><a href="../psychological.php">Психологічна підтримка</a></li>
                                    <li><a href="../medical.php">Медична допомога</a></li>
                                    <li><a href="../social.php">Соціальна допомога</a></li>
                                </ul>
                                <li><a href="../contacts.php">Контакти</a></li>
                            </ul>
                </nav>
            </div>
                <div class="row">
                    <div class="col-xs-12">

                                <?php
                                if (!empty($_SESSION)) {
                                    if (isset($_SESSION['logged']) and $_SESSION['role'] == 'admin'){
                                        ?>
                                        <nav>
                                        <ul class="nav nav-tabs">
                                            <li role="presentation"><a href="../admin.php">Profile</a></li>
                                            <li role="presentation" ><a href="../adminParser.php">AllPosts</a></li>
                                            <li role="presentation" ><a href="../parser.php">Parser</a></li>
                                        </ul>
                                        </nav>

                                <?php
                                 }
                                ?>

                        <?php
                        if (isset($_SESSION['logged']) and $_SESSION['role'] == 'moderator'){
                            ?>
                        <nav>
                            <ul class="nav nav-tabs">
                                <li role="presentation" ><a href="../adminParser.php">AllPosts</a></li>
                                <li role="presentation" ><a href="../parser.php">Parser</a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </nav>
                          <?php
                        }
                        ?>
                    </div>
                </div>
        </div>
    </div>
</header>
<body>