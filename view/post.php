<?php
session_start();

if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}

$_SESSION['init'] = true;



    if(!empty($_GET)){
        $postId=$_GET['post_id'];
        require_once "../controller/blogController.php";
        $post = new blogController();
        $onePost = $post->getPost($postId);
    }

if (!empty($_POST)) {
    if (isset($_POST['addCommUser'])) {
        if (isset($_POST['commUser']) and isset($_POST['commUserEmail']) and isset($_POST['commPassword']) and isset($_POST['re-commPassword'])) {
            $newCommUser = trim($_POST['commUser']);
            $commUserEmail = trim($_POST['commUserEmail']);
            $commPassword = trim($_POST['commPassword']);
            $reCommPassword = trim($_POST['re-commPassword']);


            if ($commPassword == $reCommPassword) {
                require_once "../controller/commentsController.php";
                $putUser = new commentsController();
                $checkedUser = $putUser->checkCommUser($newCommUser);
                $checkedUser = $checkedUser['commUsername'];
                if ($checkedUser == $newCommUser) {
                    $_SESSION['checkedUser'] = "<p>This user exist!</p>";
                } else {
                    $putUser->addNewCommU($newCommUser, $commUserEmail, $commPassword);
                }
            } else {
                echo "Wrong password";
            }
        }
    }

    if (isset($_POST['authComUser']) and isset($_POST['commUser']) and isset($_POST['commPassword']) and (!isset($_POST['commUserEmail']))) {
            $commUser = trim($_POST['commUser']);
            $commPassword = md5(trim($_POST['commPassword']));

            require_once "../controller/commentsController.php";
            $authUser = new commentsController();
            $authResult = $authUser->checkCommUserByL($commUser);
            $loggedCommUser = $authResult['login'];
            $loggedCommPass = $authResult['password'];

            if ($commUser == $loggedCommUser and $commPassword == $loggedCommPass) {
                $_SESSION['login'] = $loggedCommUser;
                $_SESSION['logged'] = true;
            }
        }
        if (isset($_POST['authComUser']) and isset($_POST['commUserEmail']) and isset($_POST['commPassword'])) {
            $commUserEmail = trim($_POST['commUserEmail']);
            $commPassword = md5(trim($_POST['commPassword']));

            require_once "../controller/commentsController.php";
            $authUser = new commentsController();
            $authResult = $authUser->checkCommUserByM($commUserEmail);
            //$loggedCommUser = $authResult['login'];
            $loggedCommUserEmail = $authResult['email'];
            $loggedCommPass = $authResult['password'];

            if ($commUserEmail == $loggedCommUserEmail and $commPassword == $loggedCommPass) {
                $_SESSION['login'] = $loggedCommUserEmail;
                $_SESSION['logged'] = true;
            }
        }


    if(isset($_POST['comments_send'])){
        $commentsText=$_POST['commentsText'];
        $author = $_SESSION['login'];
        require_once "../controller/commentsController.php";
        $addCom=new commentsController();
        $oneComm=$addCom->addComment($author, $commentsText, $postId);
    }
}
        require_once "../controller/commentsController.php";
        $comment = new commentsController();
        $comments = $comment->getComments($postId);

    require_once "header.php";

?>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2> <?=$onePost['title']?></h2>
                <?php
                if (empty($onePost['img'])) {
                    $onePost['img'] = 'assets/uploads/default.jpg';
                }
                ?>
                <img class="postImg" style="width: 100%"  src=<?='/'.$onePost['img']?> />
            </div>
        </div>

            <div class="row">
                <div class="col-xs-12">
                    <p> <?=$onePost['fullpost']?> </p>
                    <span><i class="fa fa-user" aria-hidden="true"></i> <?=$onePost['author']?></span>
                    <span><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$onePost['date']?></span>
                </div>
            </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 ">
                <div class="likes">
                    <a href="<?=$_SERVER['REQUEST_URI']?>" id="like.$postId"> <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                    <a href="<?=$_SERVER['REQUEST_URI']?>" id="like.$postId"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
              <h4> Comments</h4>
                <form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
                <div>
                    <textarea rows="2" cols="75" name="commentsText" class="commentsText" id="commentsText" autocomplete="OFF"></textarea>
                    <div class="clearfix for_rules">
                        <div class="row">
                            <?php
                                if(isset($_SESSION['login'])){
                                    ?>
                             <div class="col-xs-6  text-right">
                                <button type="submit" name="comments_send" class="btn btn-primary" id="comments_send"> коментувати</button>
                            </div>
                                    <?php
                                }
                                else {
                                    ?>
                                   <div class=" col-xs-6 col-sm-1 ">
                                        <button type="button" class="btn" data-toggle="modal"
                                                data-target="#addCommUser"> Sign up  </button>
                                   </div>
                                    <div class="col-xs-6 col-sm-2">
                                        <button type="button" class="btn" data-toggle="modal"
                                                data-target="#authComUser"> Log in  </button>
                                    </div>
                                    <?php
                                }
                                    ?>
                        </div>
                      </div>
                </div>
                </form>

                <?php
                foreach ($comments as $comment) {
                    ?>
                    <div class="comment">
                       <p><i class="fa fa-user-circle-o" aria-hidden="true"></i><b><?php echo $comment['author']; ?></b></p>
                        <p><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $comment['date']; ?></br></p>
                        <p><?php echo $comment['comment']; ?></br></p>
                    </div>

    <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>