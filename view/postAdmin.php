<?php
session_start();

if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}

$_SESSION['init'] = true;

require_once "../controller/blogController.php";

if(!empty($_GET)){
    $postId=$_GET['post_id'];
    $post = new blogController();
    $onePost = $post->getPost($postId);
}


if (!empty($_POST)) {
    if (isset($_POST['id']) and isset($_POST['show'])) {
        $id=$_POST['id'];
        $show=$_POST['show'];
        $editShow=new blogController();
        $editShow->changeShow($id, $show);
    }

    if (isset($_POST['deletePost'])) {
        $delPost = $_POST['deletePost'];
        $deletePost = new blogController();
        $deletePost->delPost($delPost);
        require('../adminParser.php');
        exit;
    }

    if (isset($_POST['editPostSend2'])) {
        $id=$_POST['editPostSend2'];
        $editTitle=$_POST['editTitle'];
        $editCategory=$_POST['editCategory'];
        $editExcerpt=$_POST['editExcerpt'];
        $editFullPost = $_POST['editFullPost'];

        if ((!empty($_FILES))) {
            if (!empty($_FILES['editFileToUpload2']['name'])) {
                $target_dir = "assets/uploads/";
                $check = getimagesize($_FILES["editFileToUpload2"]["tmp_name"]);
                if ($check !== false) {
                    $target_file = $target_dir . $_FILES["editFileToUpload2"]["name"];
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES["editFileToUpload2"]["tmp_name"], $target_file);
                }
                else {
                    echo "File is not an image.";
                }
                // Check file size
                if ($_FILES["editFileToUpload2"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                }
            }
            else {
                 $editTarget_file=$_POST['imgDb2'];
            }
        }
        $editShow=$_POST['editShow'];
        $editP = new blogController();
        $editP->editPostF($id, $editTitle, $editCategory, $editExcerpt, $editFullPost, $editTarget_file, $editShow);
        unset($editTarget_file);
        unset($_FILES["editFileToUpload2"]);
        require('../adminParser.php');
        exit;

    }
}

require_once "header.php";
?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center"> <?=$onePost['title']?></h2>
                    <?php
                    if (empty($onePost['img'])) {
                        $onePost['img'] = 'assets/uploads/default.jpg';
                    }
                    ?>
                    <img class="postImg" style="width: 100%"  src=<?='/'.$onePost['img']?> />
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <p> <?=$onePost['fullpost']?> </p>
                    <span><i class="fa fa-user" aria-hidden="true"></i> <?=$onePost['author']?></span>
                    <span><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$onePost['date']?></span>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-3 col-sm-2">
                    <?php
                    if ($onePost['show']=='show'){
                        ?>
                        <form class="input_radio">
                            <div class="radio">
                                <label><input type="radio" name="<?=$onePost['id']?>" value="hide" > Hide</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="<?=$onePost['id']?>"  value="show"  checked > Show</label>
                            </div>
                        </form>
                        <div class="result"></div>

                        <?php
                    }
                    else
                    {
                        ?>
                        <form class="input_radio">
                            <div class="radio">
                                <label><input type="radio" name="<?=$onePost['id']?>" value="hide" checked > Hide</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="<?=$onePost['id']?>"  value="show" > Show</label>
                            </div>
                        </form>
                        <div class="result"></div>

                        <?php
                    }
                    ?>
                </div>
                <div class="col-xs-3 col-sm-2">
                    <button type="button" class="btn btn " name="idPost" data-toggle="modal" data-target="#editPost" value="<?= $onePost['id'] ?>">
                        Edit
                    </button>
                    <!-- Modal Edit -->
                    <div class="modal fade" id="editPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Edit post</h4>
                                </div>
                                <div class="modal-body">

                                    <form action="postAdmin.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <textarea name="editTitle" rows="3" cols="60"><?= $onePost['title']?></textarea>
                                        </div>
                                        <select class="select" name="editCategory">
                                            <option value="Правова допомога">Правова допомога </option>
                                            <option value="Психологічна підтримка">Психологічна підтримка </option>
                                            <option value="Медична допомога">Медична допомога   </option>
                                            <option value="Психологічна підтримка">Соціальна допомога </option>
                                            <option value="Контакти">Контакти </option>
                                            <option value="Інша">Інша</option>
                                        </select>
                                        <div class="radio">
                                            <label><input type="radio" name="editShow" value="hide" checked> Hide</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="editShow" value="show"> Show</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="excerpt">Excerpt</label>
                                            <textarea name="editExcerpt" rows="6" cols="75"><?= $onePost['excerpt']?> </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="fullPost">EditFullPost</label>
                                            <textarea name="editFullPost" id="fullPost" rows="20" cols="60" > <?= $onePost['fullpost']?> </textarea>
                                            <script type="text/javascript">
                                                  CKEDITOR.replace('editFullPost');
                                            </script>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Link</label>
                                            <textarea name="editLink" rows="2" cols="60" disabled><?= $onePost['afullpost']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="dbImg">Image in DB: </label>
                                            <textarea name="imgDb2" rows="1" cols="60"><?= $onePost['img']?></textarea>
                                            <label for="img">Select another image to upload:</label>
                                            <input type="file" name="editFileToUpload2" id="editFileToUpload2" >
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button  name="editPostSend2" type="submit" class="btn btn-primary"  value="<?= $onePost['id'] ?>">Edit post</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-2">
                    <form action="/adminParser.php" method="post">
                        <div class="form-group">
                            <button name="deletePost" type="submit" value="<?= $onePost['id'] ?>"
                                    class="btn btn">Delete
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('input[type=radio]').change(function () {
                var id=this.name;
                var show=this.value;

                $.ajax({
                    url:"postAdmin.php",
                    method:"POST",
                    data:{id:id, show:show },
                    success:function () {
                    }
                })
            });
        });
    </script>
<?php
require_once "footer.php";
?>